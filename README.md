# pitastic lambda-example
This repository contains an example setup for AWS lambdas.
The goal is to enable simple creation of new lambda functions with cloudformation and then deploy and test new versions 
with lambda versions and aliases.

Big thanks belongs to this blog article:
https://blog.jayway.com/2016/07/07/continuous-deployment-aws-lambda/

## Requirements
- docker 
- make
- zip

## Setup 
### env file
Copy the env file and insert your AWS keys etc
```
cp .env.example .env
```

### Build aws container
We use a docker container with the aws cli installed to keep our environment clean
```
make docker-image
```

### Cloudformation
The cloudformation template creates an s3 bucket to upload the functions code and 2 lambdas. One helper function which is needed to upload an empty zip file while the cloudformation stack is created and the the real lambda function which will contain our code later on.

*Attention:* The lambda function will not work in the beginning. It is created with an empty file as *code*.

```
make cloudformation-lambda
```

### Update the lambda code
Now, we can start working with the lambda code itself.

All lambda code is placed inside the subdirectory 'code'. 
To compile, upload and update the lambda function execute the publish-lambda target.
```
make publish-lambda
```

With the lambda updated we can now update the different environments (dev, staging and production)
```
# update development
make push-to-development

# update staging
make push-to-staging

# update production
make push-to-production

# or update all
make push-to-all
```
