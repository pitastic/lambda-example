#!/usr/bin/env python
import json 

def response(message, status_code):
    return {
        'statusCode': str(status_code),
        'body': json.dumps(message),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
            },
        }

# return a test message
def handler(event, context):
	return response({'message': 'Big Thumbs up !!!'}, 200)
